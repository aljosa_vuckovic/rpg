import Characters.Hero;
import Characters.Mage;
import Equipment.Armor;
import Equipment.Item;
import Equipment.Slot;
import Equipment.Weapons;


public class Main {
    public static void showCharacterStatistics(Hero hero){
        System.out.println(
                hero.getName() +
                "\nHas level " + hero.getLevel()+
                "\nHas strength " + hero.getTotalPriamryAttribut().getStrength()+
                "\nHas dexterity " + hero.getTotalPriamryAttribut().getDexterity()+
                "\nHas intellect " + hero.getTotalPriamryAttribut().getIntelligence()+
                "\nHas vitality " + hero.getTotalPriamryAttribut().getVitality()+
                "\nHas  health " + hero.getSecondaryAttribut().getHealth()+
                "\nHas armor rating " + hero.getSecondaryAttribut().getArmorRating()+
                "\nHas elemental resistance " + hero.getSecondaryAttribut().getElementalResistance()+
                "\nHas DPS " + hero.getHeroDPS());
                System.out.println();
    }

    public static void main(String[] args) {

        Mage mage= new Mage("Schmitt",1);
        showCharacterStatistics(mage);
        mage.levelUp();
        showCharacterStatistics(mage);
        mage.levelUp();
        showCharacterStatistics(mage);

        Item robe= new Item("Mages robes",1, Slot.BODY, Weapons.NONE, Armor.CLOTH);
        mage.equipArmor(Slot.BODY,robe);
        showCharacterStatistics(mage);

        Item wizards_hat=new Item("Wizards hat",3,Slot.HEAD,Weapons.NONE,Armor.CLOTH);
        mage.equipArmor(Slot.HEAD,wizards_hat);
        showCharacterStatistics(mage);

        Item vocalists_kilt= new Item("Vocalists kilt",1,Slot.LEGS,Weapons.NONE,Armor.CLOTH);
        mage.equipArmor(Slot.LEGS,vocalists_kilt);
        showCharacterStatistics(mage);
    }
}
