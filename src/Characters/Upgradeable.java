package Characters;

import Equipment.Item;
import Equipment.Slot;

public interface Upgradeable  {

    boolean equipArmor(Slot slot, Item item) throws RPGExceptions;
    boolean equipWeapon(Slot slot, Item item) throws RPGExceptions;
     Item getEquipment(Slot slot);

}
