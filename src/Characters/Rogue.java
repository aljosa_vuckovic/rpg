package Characters;

import Equipment.Armor;
import Equipment.Item;
import Equipment.Slot;
import Equipment.Weapons;

public class Rogue extends Hero {

    public Rogue(String name, int level) {
        super(name, level, 2, 8, 1, 6);
    }

    @Override
    public boolean equipArmor(Slot slot, Item item) throws RPGExceptions {


        boolean wrongArmorType = true;
        boolean lvlToHigh = true;


        if (item.getArmor() != Armor.NONE
                && item.getArmor() == Armor.MAIL || item.getArmor() == Armor.LEATHER
                && item.getSlot() == slot) {
            wrongArmorType = false;
            if (this.getLevel() >= item.getLvlReg()) {
                getAllEquipment().put(slot, item);
                lvlToHigh = false;
            }

        }
        if (wrongArmorType) {
            throw new RPGExceptions("Wrong Armor type");
        } else if (!wrongArmorType) {
            if (lvlToHigh) {
                throw new RPGExceptions("Armor has to high lvl");
            }
        }
        setTotalPriamryAttribut();
        setSecondaryStats();

        if (wrongArmorType || lvlToHigh) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public boolean equipWeapon(Slot slot, Item item) throws RPGExceptions {

        boolean wrongWeaponType = true;
        boolean lvlToHigh = true;
        if (item.getWeapons() != Weapons.NONE
                && (item.getWeapons() == Weapons.DAGGERS || item.getWeapons() == Weapons.SWORDS)
                && item.getSlot() == slot) {
            wrongWeaponType = false;
            if (this.getLevel() >= item.getLvlReg()) {
                getAllEquipment().put(slot, item);
                lvlToHigh = false;
            }
        }
        if (wrongWeaponType) {
            throw new RPGExceptions("Wrong weapon type");
        } else if (!wrongWeaponType) {
            if (lvlToHigh) {
                throw new RPGExceptions("Weapon has to high lvl");
            }
        }

        setTotalPriamryAttribut();
        setSecondaryStats();
        if (wrongWeaponType || lvlToHigh) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void levelUp() {
        setBasePrimayAttribut(
                this.basePrimayAttribut.getStrength() + 1,
                this.basePrimayAttribut.getVitality() + 3,
                this.basePrimayAttribut.getIntelligence() + 1,
                this.basePrimayAttribut.getDexterity() + 4);

        setTotalPriamryAttribut();

        setSecondaryStats();

        setLevel(getLevel() + 1);
    }

    @Override
    public Item getEquipment(Slot slot) {
        return getAllEquipment().get(slot);
    }

    @Override
    public void setHeroDPS(Item weapon) {
        heroDPS = weapon.getWeaponDps() * (1 + (getTotalPriamryAttribut().getStrength() / 100));
    }

    @Override
    public double getHeroDPS() {
        return heroDPS;
    }

}
