package Equipment;

public class Item {
    private String name;
    private int lvlReg;
    private Slot slot;
    private Weapons weapons;
    private Armor armor;
    private double strength;
    private double dexterity;
    private double intelligence;
    private double vitality;
    private double weaponDamage;
    private double attacksPerSecond;
    double dps;

    public double getWeaponDps() {
        return dps;
    }

    public void setWeaponDps(double damage, double attacksPerSecond) {
        this.dps = damage * attacksPerSecond;
    }

    public Item() {
    }

    /**
     * The constructor is used to create both weapons and armor. The type is decided by the Enums Weapons and Armor
     * Depending on which Enum is set to NONE the other if statement is activated
     * (If Enum Weapons is set to Weapon.None the constructor will create stats for a Armor instead, and vice versa)
     */
    public Item(String name, int lvlReg, Slot slot, Weapons weapons, Armor armor) {
        this.name = name;
        this.lvlReg = lvlReg;
        this.slot = slot;
        this.weapons = weapons;
        this.armor = armor;
        if (armor != Armor.NONE) {
            this.strength = armor.strength;
            this.dexterity = armor.dexterity;
            this.intelligence = armor.intelligence;
            this.vitality = armor.vitality ;
        } else if (weapons != Weapons.NONE) {
            this.weaponDamage = weapons.baseDamage;
            this.attacksPerSecond = weapons.attackPerSecond;
            this.dps = weapons.baseDamage * weapons.attackPerSecond;
        }

    }

    public void setArmorStats(Armor armor) {
        if (armor != Armor.NONE) {
            this.strength = armor.strength + lvlReg;
            this.dexterity = armor.dexterity + lvlReg;
            this.intelligence = armor.intelligence + lvlReg;
            this.vitality = armor.vitality + lvlReg;
        } else {
            this.strength = 0;
            this.dexterity = 0;
            this.intelligence = 0;
            this.vitality = 0;
        }
    }

    public void setWeaponStats(Weapons weapons) {
        if (weapons != Weapons.NONE) {
            this.weaponDamage = weapons.baseDamage + lvlReg;
            if ((lvlReg - weapons.attackPerSecond) > weapons.attackPerSecond) {
                this.attacksPerSecond = lvlReg - weapons.attackPerSecond;
            } else {
                this.attacksPerSecond = weapons.attackPerSecond;
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvlReg() {
        return lvlReg;
    }

    public void setLvlReg(int lvlReg) {
        this.lvlReg = lvlReg;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public Weapons getWeapons() {
        return weapons;
    }

    public void setWeapons(Weapons weapons) {
        this.weapons = weapons;
    }

    public Armor getArmor() {
        return armor;
    }

    public void setArmor(Armor armor) {
        this.armor = armor;
    }

    public double getStrength() {
        return strength;
    }

    public double getDexterity() {
        return dexterity;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public double getVitality() {
        return vitality;
    }

    public double getWeaponDamage() {
        return weaponDamage;
    }

    public double getAttacksPerSecond() {
        return attacksPerSecond;
    }
}