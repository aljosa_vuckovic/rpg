package Equipment;

import Characters.Hero;
import Characters.RPGExceptions;
import Characters.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    @Test
    void testWeaponLvlReq() {
        Hero testWarrior = new Warrior("Schmit", 1);
        Item testAxe = new Item("Axe", 2, Slot.WEAPON, Weapons.AXES, Armor.NONE);
        assertThrows(RPGExceptions.class, () -> testWarrior.equipWeapon(Slot.WEAPON, testAxe));
    }

    @Test
    void testWeaponType() {
        Hero testWarrior = new Warrior("Schmit", 1);
        Item testAxe = new Item("Axe", 2, Slot.WEAPON, Weapons.STAFFS, Armor.NONE);
        assertThrows(RPGExceptions.class, () -> testWarrior.equipWeapon(Slot.WEAPON, testAxe));
    }

    @Test
    void testArmorLvlReq() {
        Hero testWarrior = new Warrior("Schmit", 1);
        Item testHead = new Item("Axe", 2, Slot.HEAD, Weapons.NONE, Armor.PLATE);
        assertThrows(RPGExceptions.class, () -> testWarrior.equipArmor(Slot.HEAD, testHead));
        //assertTrue();
    }

    @Test
    void testArmorType() {
        Hero testWarrior = new Warrior("Schmit", 1);
        Item testHead = new Item("Axe", 1, Slot.HEAD, Weapons.NONE, Armor.CLOTH);
        assertThrows(RPGExceptions.class, () -> testWarrior.equipArmor(Slot.HEAD, testHead));


    }

    @Test
    void testIfWeaponValid() throws RPGExceptions {
        Hero testWarrior = new Warrior("Schmitt", 1);
        Item testWeapon = new Item("Axe", 1, Slot.WEAPON, Weapons.AXES, Armor.NONE);
        assertTrue(testWarrior.equipWeapon(Slot.WEAPON, testWeapon));
    }

    @Test
    void testIfArmorValid() throws RPGExceptions {
        Hero testWarrior = new Warrior("Schmitt", 1);
        Item testArmor = new Item("Head", 1, Slot.HEAD, Weapons.NONE, Armor.PLATE);
        assertTrue(testWarrior.equipArmor(Slot.HEAD, testArmor));
    }

    @Test
    void testDPSWithNoWeapon() {
        Hero testWarrior = new Warrior("Schmitt", 1);
        assertEquals(testWarrior.getHeroDPS(), (1 * (1 + (5 / 100))));
    }

    @Test
    void testDPSWithWeaponAndWithNoEquipment() throws RPGExceptions {
        Hero testWarrior = new Warrior("Schmitt", 1);
        Item testWeapon = new Item("Axe", 1, Slot.WEAPON, Weapons.AXES, Armor.NONE);
        testWarrior.equipWeapon(Slot.WEAPON, testWeapon);
        testWarrior.setHeroDPS(testWeapon);
        assertEquals((7 * 1.1) * (1 + (5.0 / 100)), testWarrior.getHeroDPS());
    }


    @Test
    void testDPSWithWeaponAndWithEquipment() throws RPGExceptions {
        Hero testWarrior = new Warrior("Schmitt", 1.0);
        Item testArmor = new Item("Head", 1, Slot.HEAD, Weapons.NONE, Armor.PLATE);
        Item testWeapon = new Item("Axe", 1, Slot.WEAPON, Weapons.AXES, Armor.NONE);
        testWarrior.equipArmor(Slot.HEAD, testArmor);
        testWarrior.equipWeapon(Slot.WEAPON, testWeapon);
        testWarrior.setHeroDPS(testWeapon);

        double heroDPS;
        assertEquals(((7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0))), testWarrior.getHeroDPS());


    }
}