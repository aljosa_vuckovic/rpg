package Characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    void testWarrior(){
        Hero testWarrior=new Warrior("Schmitt",1);
        assertEquals(testWarrior.getLevel(),1);
    }
    @Test
    void testLevelGain(){
        Hero testWarrior= new Warrior("Schmitt",-1);

        assertThrows(IllegalArgumentException.class,()->testWarrior.levelUp());
    }

    //Warrior
    @Test
    void testWarriorBaseAttributes(){
        Hero testWarrior= new Warrior("Schmitt",1);
        double [] atrArray= new double[4];
        atrArray[0]=testWarrior.getBasePrimayAttribut().getVitality();
        atrArray[1]=testWarrior.getBasePrimayAttribut().getStrength();
        atrArray[2]=testWarrior.getBasePrimayAttribut().getDexterity();
        atrArray[3]=testWarrior.getBasePrimayAttribut().getIntelligence();

        double [] compArray={10,5,2,1};

        assertArrayEquals(atrArray,compArray);

    }

    @Test
    void testWarriorLevelUp(){
        Hero testWarrior = new Warrior("Schmitt",1);
        testWarrior.levelUp();

        double [] atrArray= new double[4];

        atrArray[0]=testWarrior.getTotalPriamryAttribut().getVitality();
        atrArray[1]=testWarrior.getTotalPriamryAttribut().getStrength();
        atrArray[2]=testWarrior.getTotalPriamryAttribut().getDexterity();
        atrArray[3]=testWarrior.getTotalPriamryAttribut().getIntelligence();

        double [] compArray= {15,8,4,2};
    }
    @Test
    void testWarriorSecondaryStats(){
        Hero testWarrior=new Warrior("Schmitt",1);
        testWarrior.levelUp();

        double [] seconAtrArray= new double[3];
        seconAtrArray[0]=testWarrior.getSecondaryAttribut().getHealth();
        seconAtrArray[1]=testWarrior.getSecondaryAttribut().getArmorRating();
        seconAtrArray[2]=testWarrior.getSecondaryAttribut().getElementalResistance();

        double [] compArray= {150,12,2};

        assertArrayEquals(seconAtrArray,compArray);

    }


    //Mage
    @Test
    void testMageBaseAttributes(){
        Hero testMage= new Mage("Schmitt",1);
        double [] atrArray= new double[4];
        atrArray[0]=testMage.getBasePrimayAttribut().getVitality();
        atrArray[1]=testMage.getBasePrimayAttribut().getStrength();
        atrArray[2]=testMage.getBasePrimayAttribut().getDexterity();
        atrArray[3]=testMage.getBasePrimayAttribut().getIntelligence();

        double [] compArray={5,1,1,8};

        assertArrayEquals(atrArray,compArray);


    }
    @Test
    void testMageLevelUp(){
        Hero testMage = new Mage("Schmitt",1);
        testMage.levelUp();

        double [] atrArray= new double[4];

        atrArray[0]=testMage.getTotalPriamryAttribut().getVitality();
        atrArray[1]=testMage.getTotalPriamryAttribut().getStrength();
        atrArray[2]=testMage.getTotalPriamryAttribut().getDexterity();
        atrArray[3]=testMage.getTotalPriamryAttribut().getIntelligence();

        double [] compArray= {8,2,2,13};
        assertArrayEquals(atrArray,compArray);



    }
    //Ranger
    @Test
    void testRangerBaseAttributes(){
        Hero testRanger= new Ranger("Schmitt",1);
        double [] atrArray= new double[4];
        atrArray[0]=testRanger.getBasePrimayAttribut().getVitality();
        atrArray[1]=testRanger.getBasePrimayAttribut().getStrength();
        atrArray[2]=testRanger.getBasePrimayAttribut().getDexterity();
        atrArray[3]=testRanger.getBasePrimayAttribut().getIntelligence();

        double [] compArray={8,1,7,1};

        assertArrayEquals(atrArray,compArray);

    }
    @Test
    void testRangerLevelUp(){
        Hero testRanger = new Ranger("Schmitt",1);
        testRanger.levelUp();

        double [] atrArray= new double[4];

        atrArray[0]=testRanger.getTotalPriamryAttribut().getVitality();
        atrArray[1]=testRanger.getTotalPriamryAttribut().getStrength();
        atrArray[2]=testRanger.getTotalPriamryAttribut().getDexterity();
        atrArray[3]=testRanger.getTotalPriamryAttribut().getIntelligence();

        double [] compArray= {10,2,12,2};
        assertArrayEquals(atrArray,compArray);


    }
    @Test
    void testRogueBaseAttributes(){
        Hero testRogue= new Rogue("Schmitt",1);
        double [] atrArray= new double[4];
        atrArray[0]=testRogue.getBasePrimayAttribut().getVitality();
        atrArray[1]=testRogue.getBasePrimayAttribut().getStrength();
        atrArray[2]=testRogue.getBasePrimayAttribut().getDexterity();
        atrArray[3]=testRogue.getBasePrimayAttribut().getIntelligence();

        double [] compArray={8,2,6,1};

        assertArrayEquals(atrArray,compArray);

    }
    @Test
    void testRogueLevelUp(){
        Hero testRogue = new Rogue("Schmitt",1);
        testRogue.levelUp();

        double [] atrArray= new double[4];

        atrArray[0]=testRogue.getTotalPriamryAttribut().getVitality();
        atrArray[1]=testRogue.getTotalPriamryAttribut().getStrength();
        atrArray[2]=testRogue.getTotalPriamryAttribut().getDexterity();
        atrArray[3]=testRogue.getTotalPriamryAttribut().getIntelligence();

        double [] compArray= {11,3,10,2};
        assertArrayEquals(atrArray,compArray);

    }
}